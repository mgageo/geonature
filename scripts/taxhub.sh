#!/bin/sh
# <!-- coding: utf-8 -->
#T geonature
# auteur: Marc Gauthier
#
# script pour mettre à jour taxhub
# https://taxhub.readthedocs.io/fr/latest/changelog.html
#
# écriture des shells de mise à jour
taxhub_1.7.0() {
  cat <<'EOFSH' > ~/taxhub_1.7.0.sh
echo DEBUT
date
# arrêt de l'application
sudo supervisorctl stop geonature2
sudo supervisorctl stop taxhub
cd ~
VERSION=$(cat ~/taxhub/VERSION)
if [ "$VERSION" != "1.6.5" ]; then
  echo "pas le bon upgrade $VERSION"
  exit -1
fi
wget https://github.com/PnX-SI/TaxHub/archive/1.7.0.zip
unzip 1.7.0.zip
mv taxhub taxhub_old
mv TaxHub-1.7.0/ taxhub
rm 1.7.0.zip
cp taxhub_old/settings.ini taxhub/settings.ini
cp taxhub_old/config.py taxhub/config.py
cp taxhub_old/static/app/constants.js taxhub/static/app/constants.js
cp -aR taxhub_old/static/medias/ taxhub/static/
cd ~/taxhub
source settings.ini
psql "host=$db_host port=5432 dbname=$db_name user=$user_pg password=$user_pg_pass" -f data/update1.6.5to1.7.0.sql
./install_app.sh
cd data/scripts/update_taxref
./import_taxref_v13_data.sh
./apply_changes.sh 13
date
sudo supervisorctl reload
echo FIN
EOFSH
}
taxhub_1.7.1() {
  cat <<'EOFSH' > ~/taxhub_1.7.1.sh
echo DEBUT
date
# arrêt de l'application
sudo supervisorctl stop geonature2
sudo supervisorctl stop taxhub
cd ~
VERSION=$(cat ~/taxhub/VERSION)
if [ "$VERSION" != "1.7.0" ]; then
  echo "pas le bon upgrade $VERSION"
  exit -1
fi
wget https://github.com/PnX-SI/TaxHub/archive/1.7.1.zip
unzip 1.7.1.zip
mv taxhub taxhub_old
mv TaxHub-1.7.1/ taxhub
rm 1.7.1.zip
cp taxhub_old/settings.ini taxhub/settings.ini
cp taxhub_old/config.py taxhub/config.py
cp taxhub_old/static/app/constants.js taxhub/static/app/constants.js
cp -aR taxhub_old/static/medias/ taxhub/static/
cd ~/taxhub
./install_app.sh
date
sudo supervisorctl reload
echo FIN
EOFSH
}
taxhub_1.7.2() {
  cat <<'EOFSH' > ~/taxhub_1.7.2.sh
echo DEBUT
date
# arrêt de l'application
sudo supervisorctl stop geonature2
sudo supervisorctl stop taxhub
cd ~
VERSION=$(cat ~/taxhub/VERSION)
if [ "$VERSION" != "1.7.1" ]; then
  echo "pas le bon upgrade $VERSION"
  exit -1
fi
wget https://github.com/PnX-SI/TaxHub/archive/1.7.2.zip
unzip 1.7.2.zip
mv taxhub taxhub_old
mv TaxHub-1.7.2/ taxhub
rm 1.7.2.zip
cp taxhub_old/settings.ini taxhub/settings.ini
cp taxhub_old/config.py taxhub/config.py
cp taxhub_old/static/app/constants.js taxhub/static/app/constants.js
cp -aR taxhub_old/static/medias/ taxhub/static/
cd ~/taxhub
# wget -P data https://github.com/PnX-SI/TaxHub/blob/master/data/update1.7.1to1.7.2.sql
source settings.ini
psql "host=$db_host port=5432 dbname=$db_name user=$user_pg password=$user_pg_pass" -f data/update1.7.1to1.7.2.sql
./install_app.sh
date
sudo supervisorctl reload
echo FIN
EOFSH
}
if [ "$(whoami)" != "geonatureadmin" ]; then
  echo "lancement sous geonatureadmin"
  exit -1
fi
# passage sous home
cd

# effacement de la verion old, compte root car dossier créé en mode sudo
[ -d ~/taxhub_old ] && sudo -u root rm -rf taxhub_old
#
# lancement
# utilisation de nohup ... & pour se prémunir des coupures de session
version=1.7.0
taxhub_${version}
nohup bash -x ~/taxhub_${version}.sh > ~/taxhub_${version}.out 2>&1 &
tail -f ~/taxhub_${version}.out

