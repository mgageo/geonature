[#postgresql_config_network]
-- configuration réseau pour permettre l'accès à distance
pg_conf=$(find /etc -name "postgresql.conf")
pg_dir=$(dirname $pg_conf)
cd $pg_dir
[ -f pg_hba.conf.mga ] || cp -pv pg_hba.conf pg_hba.conf.mga
[ -f postgresql.conf.mga ] || cp -pv postgresql.conf postgresql.conf.mga
[ -f pg_hba.conf.mga ] && cp -pv pg_hba.conf.mga pg_hba.conf
[ -f postgresql.conf.mga ] && cp -pv postgresql.conf.mga postgresql.conf
cat >> pg_hba.conf << "EOF"
# IPv4 virtual switch
host    all             all             192.168.0.0/24            md5
EOF
perl -pi  -e "s/.*listen_addresses.*#/listen_addresses = '*' #/ " postgresql.conf
systemctl restart postgresql
[#postgresql_running]
-- vérifications
ps axf | grep postgres
netstat -nltp | grep 5432
lsof -ni tcp:5432
[#debian_misc]
-- packages utiles
apt-get install -y net-tools lsof
[uname]
uname -a
[whoami]
-- qui suis-je
whoami
[espace disques]
df -h
[adresses ip]
ip addr ls
[routage ip]
ip route ls
[consommation disque]
du -s /* 2>/dev/null  | sort -n -r | head -n 20
[droits sudo]
echo "tgbtgb" | sudo -S whoami