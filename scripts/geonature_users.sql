[liste des utilistateurs]
\c geonature2db
select identifiant, nom_role, prenom_role, desc_role, pass, pass_plus from utilisateurs.t_roles;

[reset password admin]
\c geonature2db
update utilisateurs.t_roles
set pass = '21232f297a57a5a743894a0e4a801fc3', pass_plus = '2y$13$TMuRXgvIg6/aAez0lXLLFu0lyPk4m8N55NDhvLoUHh/Ar3rFzjFT.'
where identifiant = 'admin';
