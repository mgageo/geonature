#!/bin/sh
# <!-- coding: utf-8 -->
#T geonature
# auteur: Marc Gauthier
#
# script pour installer GéoNaure
#
# la sauvegarde a été transférée dans le dossier backup
#
VERSION=2.4.1
IP=`ip -f inet addr show eth0| grep -Po 'inet \K[\d.]+'`
echo "IP:$IP"
#
# récupération du script et du fichier de config
wget https://raw.githubusercontent.com/PnX-SI/GeoNature/${VERSION}/install/install_all/install_all.ini
wget https://raw.githubusercontent.com/PnX-SI/GeoNature/${VERSION}/install/install_all/install_all.sh
#
# personnalisation
perl -pi -e 's|^(my_url=http://).*|my_url=http://${IP}/|' install_all.ini
perl -pi -e 's/^(user_pg_pass=).*/$1geonature/' install_all.ini
more install_all.ini
#
# lancement de l'installation
touch install_all.log
chmod +x install_all.sh
./install_all.sh 2>&1 | tee install_all.log


