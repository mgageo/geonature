# <!-- coding: utf-8 -->
#
# quelques fonctions pour Geonature
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
mga  <- function() {
  source("geonature/scripts/geonature.R");
}
#
# les commandes permettant le lancement
Drive <- substr( getwd(),1,2)
baseDir <- sprintf("%s/web", Drive)
cfgDir <- sprintf("%s/web/geonature/GEONATURE", Drive)
scriptsDir <- sprintf("%s/web/geonature/scripts", Drive)
texDir <- sprintf("%s/web/geonature/GEONATURE", Drive)
dir.create(cfgDir, showWarnings = FALSE, recursive = TRUE)
dir.create(texDir, showWarnings = FALSE, recursive = TRUE)
setwd(baseDir)
source("geonature/config/mga.R");
source("geo/scripts/misc.R");
source("geonature/scripts/geonature_compare.R");
source("geonature/scripts/geonature_install.R");
source("geonature/scripts/geonature_misc.R");
source("geonature/scripts/misc_postgresql.R");
source("geonature/scripts/geonature_stations.R");
if ( interactive() ) {
  print(sprintf("geonature.R interactif"))
# un peu de nettoyage
  graphics.off()
} else {
  print(sprintf("geonature.R console"))
}
