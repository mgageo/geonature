#!/bin/sh
# <!-- coding: utf-8 -->
#T geonature
# auteur: Marc Gauthier
#
# script pour restaurer une sauvegarde
#
# la sauvegarde a été transférée dans le dossier backup
#
sudo supervisorctl stop geonature2
sudo supervisorctl stop taxhub
sudo -n -u postgres -s dropdb geonature2db
sudo -n -u postgres -s createdb -O geonatadmin geonature2db
sudo -n -u postgres -s psql -d geonature2db -c "CREATE EXTENSION IF NOT EXISTS postgis;"
sudo -n -u postgres -s psql -d geonature2db -c "CREATE EXTENSION IF NOT EXISTS hstore;"
sudo -n -u postgres -s psql -d geonature2db -c "CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog; COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';"
sudo -n -u postgres -s psql -d geonature2db -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'
sudo -n -u postgres -s psql -d geonature2db -c "CREATE EXTENSION IF NOT EXISTS pg_trgm;"
# le fichier appartient à l'utilisateur unix postgres
 la restauration dure deux à trois minutes
date
sudo su postgres -c 'pg_restore -d geonature2db backup/mercredi-geonaturedb.backup > /tmp/pg_restore.out 2>&1'
date
sudo su postgres  -c 'vacuumdb geonature2db'
sudo supervisorctl reload

