#!/bin/sh
# <!-- coding: utf-8 -->
#T geonature
# auteur: Marc Gauthier
#
# script pour postgresql
#
# la configuration réseau
#
if [ $EUID -ne 0 ]; then
  echo "Le script doit etre lancé en root"
  exit 1
fi
#
# pour permettre l'accès à distance
pg_conf=$(find /etc -name "postgresql.conf")
pg_dir=$(dirname $pg_conf)
cd $pg_dir
[ -f pg_hba.conf.mga ] || cp -pv pg_hba.conf pg_hba.conf.mga
[ -f postgresql.conf.mga ] || cp -pv postgresql.conf postgresql.conf.mga
[ -f pg_hba.conf.mga ] && cp -pv pg_hba.conf.mga pg_hba.conf
[ -f postgresql.conf.mga ] && cp -pv postgresql.conf.mga postgresql.conf
cat >> pg_hba.conf << "EOF"
# IPv4 virtual switch
host    all             all             192.168.0.0/24            md5
EOF
perl -pi  -e "s/.*listen_addresses.*#/listen_addresses = '*' #/ " postgresql.conf
systemctl restart postgresql


