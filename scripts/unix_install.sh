#!/bin/sh
# <!-- coding: utf-8 -->
#T geonature
# auteur: Marc Gauthier
#
# script pour installer GéoNaure
#
# la configuration unix
#
if [ $EUID -ne 0 ]; then
  echo "Le script doit etre lancé en root"
  exit 1
fi
#
# mise à jour de la distribution
apt-get update
apt-get upgrade -y
apt-get autoremove && apt-get autoclean
#
#
apt-get install -y sudo ca-certificates
perl -pi.mga -e 's{env_reset.*$}{env_reset,timestamp_timeout=-1}' /etc/sudoers
service sudo restart
sudo -l
#
# création de l'utilsateur geonatureadmin
adduser geonatureadmin
#
# ajout au groupe sudo
adduser geonatureadmin sudo

