#!/bin/sh
# <!-- coding: utf-8 -->
#T geonature
# auteur: Marc Gauthier
#
# script pour limiter les communes à une emprise
# https://github.com/PnX-SI/Ressources-techniques/blob/master/GeoNature/V2/clean_areas.sql
#
# chargement de l'emprise
shp2pgsql -I -s 2154 ../emprise/empriseL93.shp empriseL93 | psql "$DB"
#
# pour voir la table
psql "$DB" '\d ref_geo.li_municipalities'
psql "$DB" '\d ref_geo.l_areas'
cat <<'EOF' > clone.sql
CREATE TABLE li_municipalities AS
TABLE ref_geo.li_municipalities;

SELECT area_name, ST_AsText(geom)
FROM ref_geo.l_areas
LIMIT(3);


SELECT ref_geo.l_areas.*, empriseL93.*
FROM ref_geo.l_areas
JOIN empriseL93
ON ST_Intersects(ref_geo.l_areas.centroid, empriseL93.geom)
;
